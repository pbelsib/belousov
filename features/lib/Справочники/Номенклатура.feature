#language: ru
@IgnoreOnCIMainBuild
@Tree
@ExportScenarios

Функционал: Создание Номенклатуры

Как Ответственный за Номенклатуру
Я хочу
Чтобы создавать Номенклатуру в нужной группе

Сценарий: Создание Группы Товаров "Группа_Товаров"
	Если Отсутствует элемент справочника "Номенклатура" с наименованием "Группа_Товаров" тогда
		И я закрыл все окна клиентского приложения
		И В командном интерфейсе я выбираю 'НСИ и администрирование' 'Номенклатура'
		Тогда открылось окно 'Номенклатура'
		И я нажимаю на кнопку 'Изменить вариант навигации'
		И я выбираю пункт меню 'Навигация по иерархии'
		И я выбираю пункт контекстного меню с именем 'ИерархияНоменклатурыКонтекстноеМенюСоздатьГруппу' на элементе формы с именем "ИерархияНоменклатуры"
		Тогда открылось окно 'Номенклатура (создание группы)'
		И в поле 'Наименование' я ввожу текст 'Группа_Товаров'
		И я нажимаю кнопку очистить у поля "Входит в группу"
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Номенклатура (создание группы)' в течение 20 секунд
	Тогда КонецЕсли


Сценарий: Создание Номенклатуры "Имя_Номенклатуры" в Группе "Группа_Номенклатуры"

	Если Отсутствует элемент справочника "Номенклатура" с наименованием "Имя_Номенклатуры" тогда
		И я закрыл все окна клиентского приложения
		И В командном интерфейсе я выбираю 'НСИ и администрирование' 'Номенклатура'
		Тогда открылось окно 'Номенклатура'
		И я нажимаю на кнопку с именем 'СписокРасширенныйПоискНоменклатураСоздать'
		Тогда открылось окно 'Номенклатура (создание)'
		И в поле 'Рабочее наименование' я ввожу текст 'Имя_Номенклатуры'
		И я нажимаю кнопку выбора у поля с именем "ВидНоменклатурыОбязательныеПоля"
		Тогда открылось окно 'Виды номенклатуры'
		И в таблице "Список" я перехожу к строке:
			| 'Наименование'  |
			| 'Группа Товары' |
		И в таблице "Список" я выбираю текущую строку
		И в таблице "Список" я перехожу к строке:
			| 'Наименование'      |
			| 'Штучный Товар 18%' |
		И в таблице "Список" я выбираю текущую строку

		И я меняю значение переключателя 'НастройкаВидимостиФормы' на 'Показать все'
		И я нажимаю кнопку выбора у поля "Группа списка"
		Тогда открылось окно 'Номенклатура'
		И в таблице "Список" я перехожу к строке:
			| 'Наименование' |
			| 'Группа_Номенклатуры'   |
		И я нажимаю на кнопку с именем 'ФормаВыбрать'

		Тогда открылось окно 'Номенклатура (создание) *'
		И я нажимаю на кнопку 'Записать и закрыть'
		И я жду закрытия окна 'Номенклатура (создание) *' в течение 20 секунд
	Тогда КонецЕсли
